/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controller;

import com.mycompany.dao.HotelDAO;
import com.mycompany.entity.Hotel;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author safrin
 */
@Controller
public class HotelController {

    @Autowired
    private HotelDAO hotelDAO;

    @RequestMapping("/searchHotels")
    public ModelAndView search(@RequestParam(required = false, defaultValue = "") String location) {
        ModelAndView mav = new ModelAndView("showHotels");
        List<Hotel> hotels = location.trim().isEmpty() ? hotelDAO.getAll() : hotelDAO.search(location.trim());
        mav.addObject("SEARCH_HOTELS", hotels);
        return mav;
    }

    @RequestMapping("/viewAllHotels")
    public ModelAndView getAll() {
        ModelAndView mav = new ModelAndView("showHotels");
        List<Hotel> hotels = hotelDAO.getAll();
        mav.addObject("SEARCH_HOTELS", hotels);
        return mav;
    }

    @RequestMapping(value = "/saveHotel", method = RequestMethod.GET)
    public ModelAndView newuserForm() {
        ModelAndView mav = new ModelAndView("newHotel");
        Hotel hotel = new Hotel();
        mav.getModelMap().put("newHotel", hotel);
        return mav;
    }

    @RequestMapping(value = "/saveHotel", method = RequestMethod.POST)
    public String create(@ModelAttribute("newHotel") Hotel hotel, BindingResult result, SessionStatus status) {

        hotelDAO.save(hotel);
        status.setComplete();
        return "redirect:viewAllHotels.do";
    }

    @RequestMapping(value = "/updateHotel", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam("id") Integer id) {
        ModelAndView mav = new ModelAndView("editHotel");
        Hotel hotel = hotelDAO.getById(id);
        mav.addObject("editHotel", hotel);
        return mav;
    }

    @RequestMapping(value = "/updateHotel", method = RequestMethod.POST)
    public String update(@ModelAttribute("editHotel") Hotel hotel, BindingResult result, SessionStatus status) {
        hotelDAO.update(hotel);
        status.setComplete();
        return "redirect:viewAllHotels.do";
    }

    @RequestMapping("/deleteHotel")
    public ModelAndView delete(@RequestParam("id") Integer id) {
        ModelAndView mav = new ModelAndView("redirect:viewAllHotels.do");
        hotelDAO.delete(id);
        return mav;
    }

    public HotelDAO getHotelDAO() {
        return hotelDAO;
    }

    public void setHotelDAO(HotelDAO hotelDAO) {
        this.hotelDAO = hotelDAO;
    }

}
