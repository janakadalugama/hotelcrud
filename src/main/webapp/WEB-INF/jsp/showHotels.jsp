<%@include file="taglib_includes.jsp" %>

<html>
    <head>
        <script src="<c:url value='/resources/js/func.js' />" type="text/javascript"></script>      
        <title>Hotel Management</title>

    </head>
    <body style="font-family: Arial; font-size:smaller;background: activecaption;">

    <center >
        <div style="padding-top: 10%;">
            <h1>Hotel Management</h1>
            <form action="searchHotels.do" method="post">
                <table  style="border-collapse: collapse; width: 500px;border: 0;border-color:#006699;">
                    <tbody>
                        <tr>    
                            <td>Enter Hotel Location</td>     
                            <td><input name="location" type="text">
                                <input value="Search" type="submit">
                                <input onclick="javascript:go('saveHotel.do');" value="New Hotel" type="button">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>

            <br/><br/>
            <table style="border-collapse: collapse; width: 500px;border: 1px;background: lightblue;">
                <tbody>
                    <tr >  
                        <th>Id</th>    
                        <th>Name</th>
                        <th>Address</th> 
                        <th>Location</th>
                        <th></th>
                    </tr>
                    <c:if test="${empty SEARCH_HOTELS}">
                        <tr><td colspan="4">No Results found</td></tr>
                    </c:if>

                    <c:if test="${! empty SEARCH_HOTELS}">    
                        <c:forEach items="${SEARCH_HOTELS}" var="contact">
                            <tr>   
                                <td><c:out value="${contact.id}"></c:out></td>  
                                <td><c:out value="${contact.name}"></c:out></td>   
                                <td><c:out value="${contact.address}"></c:out> </td>  
                                <td><c:out value="${contact.location}"></c:out></td>   
                                <td><a href="javascript:go('updateHotel.do?id=${contact.id}');">Edit</a>
                                    <a href="javascript:deleteHotel('deleteHotel.do?id=${contact.id}');">Delete</a></td> 
                            </tr>
                        </c:forEach>
                    </c:if>
                </tbody>
            </table>
        </div>
    </center>

</body>
</html>
